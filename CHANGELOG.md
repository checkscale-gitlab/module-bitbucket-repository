0.2.0
=====

* maintenance: pins bitbucket provider to 2+
* chore: pins pre-commit dependencies

0.1.1
=====

* fix: makes sure checks on `var.branch_protection_overrides` checks key, not value

0.1.0
=====

* feat: adds is_private, has_wiki variables to control repository creation

0.0.0
=====

 * tech: initialise repository
